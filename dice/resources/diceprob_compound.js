
var expertiseReroll = 0;
var curGroup;

function resultCopy(newresult, result)
{
	newresult.prob = result.prob;
	newresult.success = result.success;
}

function probChallenge(result, n)
{
	var nr = new Object();

	resultCopy(nr, result);
	nr.prob *= 0.25; 	// double miss
	nr.success -= 2;
	nextProbDice(nr, n+1);

	resultCopy(nr, result);
	nr.prob *= 0.25; 	// miss
	nr.success -= 1;
	nextProbDice(nr, n+1);

	resultCopy(nr, result);
	nr.prob *= 0.5; 	// other results
	nextProbDice(nr, n+1);
}

function probCharacteristic(result, n)
{
	var nr = new Object();

	resultCopy(nr, result);
	nr.prob *= 0.5; 	// hit
	nr.success += 1;
	nextProbDice(nr, n+1);

	resultCopy(nr, result);
	nr.prob *= 0.5; 	// blank
	nextProbDice(nr, n+1);
}

function probConservative(result, n)
{
	var nr = new Object();

	resultCopy(nr, result);
	nr.prob *= 0.7; 	// hit
	nr.success += 1;
	nextProbDice(nr, n+1);

	resultCopy(nr, result);
	nr.prob *= 0.3; 	// blank
	nextProbDice(nr, n+1);
}

function probReckless(result, n)
{
	var nr = new Object();

	resultCopy(nr, result);
	nr.prob *= 0.2; 	// double hit
	nr.success += 2;
	nextProbDice(nr, n+1);

	resultCopy(nr, result);
	nr.prob *= 0.3; 	// hit
	nr.success += 1;
	nextProbDice(nr, n+1);

	resultCopy(nr, result);
	nr.prob *= 0.5; 	// blank
	nextProbDice(nr, n+1);
}

function probExpertise(result, n)
{
	var nr = new Object();

	resultCopy(nr, result);
	nr.prob *= 1/6; 	// hit + reroll
	if (expertiseReroll < 1)
	{
		nr.success += 1;
		expertiseReroll++;
		nextProbDice(nr, n);
	}
	else {
		nr.success += 2;
		expertiseReroll = 0;
		nextProbDice(nr, n+1);	
	}

	resultCopy(nr, result);
	nr.prob *= 2/6; 	// hit or comet
	nr.success += 1;
	nextProbDice(nr, n+1);

	resultCopy(nr, result);
	nr.prob *= 3/6; 	// blank
	nextProbDice(nr, n+1);
}

function probFortune(result, n)
{
	var nr = new Object();

	resultCopy(nr, result);
	nr.prob *= 2/6; 	// hit
	nr.success += 1;
	nextProbDice(nr, n+1);

	resultCopy(nr, result);
	nr.prob *= 4/6; 	// blank
	nextProbDice(nr, n+1);
}

function probMisfortune(result, n)
{
	var nr = new Object();

	resultCopy(nr, result);
	nr.prob *= 2/6; 	// miss
	nr.success -= 1;
	nextProbDice(nr, n+1);

	resultCopy(nr, result);
	nr.prob *= 4/6; 	// blank
	nextProbDice(nr, n+1);
}

function nextProbDice(result, n)
{
	if (n === curGroup.numDice)
	{
		curGroup.sprob[result.success+10] += result.prob;
		return;
	}

	switch(curGroup.pool[n])
	{
		case 0: probChallenge(result, n); break;
		case 1: probCharacteristic(result, n); break;
		case 2: probConservative(result, n); break;
		case 3: probReckless(result, n); break;
		case 4: probExpertise(result, n); break;
		case 5: probFortune(result, n); break;
		case 6: probMisfortune(result, n); break;
		default: alert('what'); break;
	}
}

function precHack (val)
{
	val2 = Math.round(val*10000);
	if (val2 < 10) return '0.000' + val2;
	if (val2 < 100) return '0.00' + val2;
	if (val2 < 1000) return '0.0' + val2;
	if (val2 < 10000) return '0.' + val2;
	else return val2 / 10000 + '.0000';
}

function crunchProbArrays(prob1, prob2)
{
	prob1[5] += prob1[0] + prob1[1] + prob1[2] + prob1[3] + prob1[4];
	prob1[15] += prob1[16] + prob1[17] + prob1[18] + prob1[19] + prob1[20];
	prob2[5] += prob2[0] + prob2[1] + prob2[2] + prob2[3] + prob2[4];
	prob2[15] += prob2[16] + prob2[17] + prob2[18] + prob2[19] + prob2[20];

	var j,k;
	var prob = []; for(j=0; j<21; j++) prob[j] = 0;

	for(j=5; j<16; j++)
		for (k=5; k<16; k++)
				prob[j+k-10] += prob1[j] * prob2[k];

	for(j=0; j<21; j++) prob1[j] = prob[j];
}

function updateProb()
{
	// split dicepool into groups
	var groupList = [];
	var i,j,k; var numGroups=0;
	for(i=0; i<numDice; i+=4)
	{
		var group = new Object;
		groupList[numGroups++] = group;
		group.pool = [dicePool[i], dicePool[i+1], dicePool[i+2], dicePool[i+3]];
		if(i+4 <= numDice) group.numDice = 4; else group.numDice = numDice-i;
		group.sprob = []; for(j=0; j<21; j++) group.sprob[j] = 0;
	}
	
	// generate probability list for each group
	for(i=0; i<numGroups; i++)
	{
		var result = new Object();
		result.prob = 1.0;
		result.success = 0;
		curGroup = groupList[i];
		nextProbDice(result, 0);
	}

	// crunch groups
	for(i=1; i<numGroups; i++)
		crunchProbArrays(groupList[0].sprob, groupList[i].sprob);

	// write final value
	var prob1Hit = 0, prob2Hit = 0, prob3Hit = 0;
	if(numDice !== 0) {
		for (i=11; i<21; i++) prob1Hit += groupList[0].sprob[i];
		prob2Hit = prob1Hit - groupList[0].sprob[11];
		prob3Hit = prob2Hit - groupList[0].sprob[12];
	}
	$('#prob0HitPC').text(Math.round((1-prob1Hit)*100) + '%');
	$('#prob1HitPC').text(Math.round(prob1Hit*100) + '%');
	$('#prob2HitPC').text(Math.round(prob2Hit*100) + '%');
	$('#prob3HitPC').text(Math.round(prob3Hit*100) + '%');
}

