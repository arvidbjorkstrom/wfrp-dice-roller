
var numDice = 0;
var dicePool = [];
var diceCount = [0, 0, 0, 0, 0, 0, 0];
//var diceNumbers = { 'challenge':0, 'characteristic':1, 'conservative':2, 'reckless':3, 'expertise':4, 'fortune':5, 'misfortune':6 }
var diceNumbers = { 'cl':0, 'cr':1, 'co':2, 're':3, 'ex':4, 'fo':5, 'mi':6 }
var diceNames = ['challenge', 'characteristic', 'conservative', 'reckless', 'expertise', 'fortune', 'misfortune']
var shortNames = ['cl', 'cr', 'co', 're', 'ex', 'fo', 'mi'];

var numRes = 0;
var diceRes1 = [];
var diceRes2 = [];

var diceResults = ['blank', 'hammer', 'sword', 'eagle', 'skull', 'star', 'hourglass', 'blood', 'hammer_plus', 'comet']

var challengeDie = [2,2, 2,2, 2,0, 2,0, 4,4, 4,0, 5,0, 0,0];
var characteristicDie = [1,0, 1,0, 1,0, 1,0, 3,0, 3,0, 0,0, 0,0];
var conservativeDie = [1,0, 1,0, 1,0, 1,0, 1,3, 1,6, 1,6, 3,0, 3,0, 0,0];
var recklessDie = [1,1, 1,1, 1,7, 1,7, 1,3, 3,3, 4,0, 4,0, 0,0, 0,0];
var expertiseDie = [1,0, 8,0, 3,0, 3,0, 9,0, 0,0];
var fortuneDie = [1,0, 1,0, 3,0, 0,0, 0,0, 0,0];
var misfortuneDie = [2,0, 2,0, 4,0, 0,0, 0,0, 0,0];

var dieTable = [challengeDie, 8, characteristicDie, 8, conservativeDie, 10, recklessDie, 10, expertiseDie, 6, fortuneDie, 6, misfortuneDie, 6];

function ithingyHax()
{
	if (ithingyHax.done === true) return;
	ithingyHax.done = true;
	$('body').addClass('nohover');
	$('.button,.dice_selector').unbind('click');
}

$(function () {
	var moved = false;
	clearPool();

	$('.button,.dice_selector').bind('touchstart', {}, function(event) { moved = false; ithingyHax(); }).
	bind('touchmove', {}, function(event) { moved = true; });

	$('.button_clear').bind('click touchend', {}, function(event) { if(!moved) clearPool(); });
	$('.button_roll').bind('click touchend', {}, function(event) { if(!moved) rollAllDice(); });

//		$('#'+event.target.id).addClass('button_pressed');
//		setTimeout('$(\'#'+event.target.id+'\').removeClass("button_pressed");',5000);

	$('.dice_selector').bind('click touchend', {}, function(event) {
		if(moved) return;
		var t = diceNumbers[event.target.id.substring(5, 7)];
		var n = parseInt(event.target.id.substring(7));
		setDice(t, n);
	});
	
	for(i=0;i<7;i++)
	{
		if($(document).getUrlParam(shortNames[i])) setDice(i, $(document).getUrlParam(shortNames[i]));
	}
	$('body').scrollTop(1);
});

function playSound(soundobj)
{
	document.getElementById(soundobj).Play();
}


function rollAllDice()
{
//	playSound('soundclick');
	numRes = numDice;
	var i;
	for(i=0; i<numDice; i++)
	{
		var curDie = dieTable[dicePool[i]*2];
		var dsize = dieTable[dicePool[i]*2+1];
		r = Math.floor(Math.random()*0.999999*dsize);
		diceRes1[i] = curDie[r*2];
		diceRes2[i] = curDie[r*2+1];

		while(curDie[r*2] === 8)			// hammer+ reroll
		{
			r = Math.floor(Math.random()*0.999999*dsize);
			diceRes1[numRes] = curDie[r*2];
			diceRes2[numRes] = curDie[r*2+1];
			dicePool[numRes] = 4;		// set to expertise color
			numRes++;
		}
	}
	updateDiceImages();
	updateResultImages();
}

function updateDiceImages()
{
	$('#dice_pool').text('');
	var i; for(i=0; i<numRes; i++) 
	{
		var color = '_white'; if (dicePool[i] === 4 || dicePool[i] === 5) color = '_black';
		var string = '<div class="dicepool dsel_' + diceNames[dicePool[i]] + '">\n';
		if (diceRes2[i] === 0) string += '<img class="largeface" src="img/' + diceResults[diceRes1[i]] + color + '.png"/>';
		else {
			string += '<img class="smallface" src="img/' + diceResults[diceRes1[i]] + color + '.png"/>';
			string += '<img class="smallface" src="img/' + diceResults[diceRes2[i]] + color + '.png"/>';
		}
		string += '</div>\n';

		$('#dice_pool').append(string);
	}
	updateProb();
}

function makeResult(dclass, image)
{
	string = '<div class="dsel_' + dclass + ' diceresult">';
	string += '<img class="resultface" src="img/' + image + '.png"/></div>';
	return string;
}

function updateResultImages()
{
	// merge results arrays
	var allres = [];
	var i; for(i=0; i<numRes; i++) {
		allres[i*2] = diceRes1[i];
		allres[i*2+1] = diceRes2[i];
	}
	var success=0, boon=0, special=0;
	var specres = []; var specdice = [];

	// count results
	var i; for(i=0; i<numRes*2; i++)
	{
		switch(allres[i])
		{
			case 1: case 8: success += 1; break;
			case 2: success -= 1; break;
			case 3: boon += 1; break;
			case 4: boon -= 1; break;
			case 5: case 6: case 7: case 9:
				specdice[special] = dicePool[Math.floor(i/2)];
				specres[special++] = allres[i]; break;
			default: break;
		}
	}

	var string;
	$('#successtally').text('');
	$('#boontally').text('');
	$('#specialtally').text('');

	if(success === 0) $('#successtally').html('&nbsp;');
	else {
		if (success > 0) string = makeResult('fortune', 'hammer_black');
		else { success = -success; string = makeResult('misfortune', 'sword_white'); }
		if(success>8)
			$('#successtally').html(string + '&nbsp;x ' + success);
		else
			for(i=0; i<success && i<8; i++) {
				$('#successtally').append(string);
		}
	}

	if(boon === 0) $('#boontally').html('&nbsp;');
	else {
		if (boon > 0) string = makeResult('fortune', 'eagle_black');
		else { boon = -boon; string = makeResult('misfortune', 'skull_white'); }
		if(boon>8)
			$('#boontally').html(string + '&nbsp;x ' + boon);
		else
			for(i=0; i<boon && i<8; i++) {
				$('#boontally').append(string);
		}
	}

	if(special === 0) $('#specialtally').html('&nbsp;');
	else {
		for(i=0; i<special; i++) {
			var color = '_white'; if (specdice[i] === 4 || specdice[i] === 5) color = '_black';
			string = makeResult(diceNames[specdice[i]], diceResults[specres[i]] + color);
			$('#specialtally').append(string);
		}
	}
}

function clearResultImages()
{
	$('#successtally').html('&nbsp;');
	$('#boontally').html('&nbsp;');
	$('#specialtally').html('&nbsp;');
}

function updateDicePool()
{
	numDice = 0; dicePool = [];
	var i, j; for(i=0; i<7; i++) 
		for(j=0; j<diceCount[i]; j++) dicePool[numDice++] = i;

	// clear results table
	for(i=0; i<numDice; i++) diceRes1[i] = diceRes2[i] = 0;
	numRes = numDice;
}

function setDice(type, number)
{
	if (diceCount[type] === number) return;
	setDiceInternal(type, number);

	updateDicePool();
	updateDiceImages();
	clearResultImages();
}

function setDiceInternal(type, number)
{
	if (type === 2 && number !== 0) setDiceInternal(3, 0);
	if (type === 3 && number !== 0) setDiceInternal(2, 0);

	// update dicePool
	var name = diceNames[type];
	var sel = '#dice_' + shortNames[type];
	$(sel + diceCount[type]).removeClass('dsel_' + name + '2');
	$(sel + diceCount[type]).addClass('dsel_' + name);
	diceCount[type] = number;
	$(sel + number).removeClass('dsel_' + name);
	$(sel + number).addClass('dsel_' + name + '2');
}

function clearPool()
{
	var i;
	for (i=0; i<7; i++) setDiceInternal(i, 0);
	updateDicePool();
	updateDiceImages();
	clearResultImages();
}
